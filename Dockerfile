FROM openjdk:11
ADD target/sistema-externo-legado.jar sistema-externo-legado.jar
ENTRYPOINT ["java", "-jar","sistema-externo-legado.jar"]
EXPOSE 8080
