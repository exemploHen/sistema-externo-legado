package br.com.parceiro.sistemaexternolegado.dto.request;

public class SolicitacaoAtendimentoRequestDTO {

  private String parametro;
  private Integer numero;

  public String getParametro() {
    return parametro;
  }

  public void setParametro(String parametro) {
    this.parametro = parametro;
  }

  public Integer getNumero() {
    return numero;
  }

  public void setNumero(Integer numero) {
    this.numero = numero;
  }
}
