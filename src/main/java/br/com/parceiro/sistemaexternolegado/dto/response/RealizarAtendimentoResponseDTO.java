package br.com.parceiro.sistemaexternolegado.dto.response;

public class RealizarAtendimentoResponseDTO {

  private String mensagem;
  private Integer code;


  public RealizarAtendimentoResponseDTO() {
  }

  public RealizarAtendimentoResponseDTO(String mensagem, Integer code) {
    this.mensagem = mensagem;
    this.code = code;
  }

  public String getMensagem() {
    return mensagem;
  }

  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }
}
