package br.com.parceiro.sistemaexternolegado.controller;

import br.com.parceiro.sistemaexternolegado.dto.request.SolicitacaoAtendimentoRequestDTO;
import br.com.parceiro.sistemaexternolegado.dto.response.RealizarAtendimentoResponseDTO;
import br.com.parceiro.sistemaexternolegado.service.ProcessamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("atendimento")
public class AtendimentoController {

  private final ProcessamentoService processamentoService;

  @Autowired
  public AtendimentoController(ProcessamentoService processamentoService) {
    this.processamentoService = processamentoService;
  }

  @PostMapping
  public ResponseEntity<RealizarAtendimentoResponseDTO> realizarAtendimento(@RequestBody SolicitacaoAtendimentoRequestDTO requestDTO) {
    RealizarAtendimentoResponseDTO responseDTO;

    if (processamentoService.processarAtendimento()) {
      responseDTO = new RealizarAtendimentoResponseDTO("Sucesso!", 200);
      return ResponseEntity.ok(responseDTO);
    } else {
      responseDTO = new RealizarAtendimentoResponseDTO("Sou frágil!!! Favor tentar novamente com carinho!!", 500);
      return ResponseEntity.internalServerError().body(responseDTO);
    }
  }
}
