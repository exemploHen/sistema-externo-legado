package br.com.parceiro.sistemaexternolegado.service;

import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class ProcessamentoService {

  private final Random sistemaFunciona = new Random();
  private static final int tempoMinimo = 100;
  private static final int tempoMaximo = 60000;

  public boolean processarAtendimento() {
    try {
      Thread.sleep(tempoProcessamento());
      return sistemaFunciona.nextBoolean();
    } catch (InterruptedException e) {
      return false;
    }
  }

  private long tempoProcessamento() {
    return (long) (sistemaFunciona.nextInt(tempoMaximo - tempoMinimo) + tempoMinimo);
  }

}
