package br.com.parceiro.sistemaexternolegado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaConsumidorLegadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaConsumidorLegadoApplication.class, args);
	}

}
