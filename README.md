# Configuração da aplicação
### O que fazer para rodar a aplicação:

1. Clone o projeto para sua máquina local [Repositório Git](https://gitlab.com/exemploHen/sistema-externo-legado.git)
    - git clone git@gitlab.com:exemploHen/sistema-externo-legado.git
3. Construa a aplicação utilizando o [Maven](https://maven.apache.org/) no diretório do arquivo pom.xml
    - mvn clean package
4. Execute o ambiente [Docker](https://www.docker.com/) no mesmo diretório que o arquivo docker-compose.yml o comando:
    - docker-compose up --build
5. Pronto!!! A aplicação já está rodando!!!!!

### Acesso a aplicação
Para acessar a aplicação, via [Postman](https://www.postman.com/) ou página:

* Documentação pelo Postman, importação pelo [link](https://www.getpostman.com/collections/577f3ca8e98dfbcaf167)
